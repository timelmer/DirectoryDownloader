<?php
// SETTINGS
$depth = 3; // How many directory layers should be added to the zip file. Default 3
$tempLocation = 'temp/';  // Where to store the zip file while being created. Default 'temp/'
ini_set('max_execution_time', 120); // How long the zip should be allowed to run. Default 2m

// Path parser from Minixed @ https://github.com/lorenzos/Minixed
$_target = null; // The target directory
$_GET['b'] = trim(str_replace('\\', '/', $_GET['b']), '/ ');
$_GET['b'] = str_replace(array('/..', '../'), '', $_GET['b']); // Avoid going up into filesystem
if (!empty($_GET['b']) && $_GET['b'] != '..' && is_dir($_GET['b'])) $_target = $_GET['b'];

// Check target
if (!file_exists($_target))
    displayError("File does not exit");

$_zip = new ZipArchive();   // The zip archive

// Make temp directory if not present
if (!file_exists($tempLocation) && !mkdir($tempLocation))
    displayError("Could not create temp directory");

$_zipFileName = str_replace( '/','_' , $_target). '.zip';  // The name of the zip archive

// Create zip
$_res = $_zip->open($tempLocation . $_zipFileName, ZipArchive::CREATE);
if ($_res !== true)
    displayError("Could not open zip file: " . $_res);

// Fill zip
if (!addFileToZip('./' . $_target, $_zip, $depth, './' . $_target))
    displayError("Could not add file(s) to zip file");

// Write zip to disk
if (!$_zip->close())
    displayError("Could not close zip file");

if (file_exists($tempLocation . $_zipFileName))
{
    // Setup file for download, rather than text display
    header('Content-Type: application/zip');
    header('Content-Disposition: attachment; filename="' . $_zipFileName . '"');
    header('Content-Length: ' . filesize($tempLocation . $_zipFileName));

    // Open file for client
    readfile($tempLocation . $_zipFileName);

    // Delete file
    unlink($tempLocation . $_zipFileName);
}
else
    displayError("Zip file no longer exists");

/**
 * @param $file string Path to file to add to zip (from CWD)
 * @param $zip ZipArchive Archive to add files to
 * @param $depth int Number of levels to descend from each file
 * @param $root string Path to zip root
 * @return bool Success
 */
function addFileToZip($file, &$zip, $depth, $root)
{
    // Escape if beyond allowed depth
    if ($depth < 0)
        return true;

    if (!is_dir($file))
        // Add file with name sans root
        return $zip->addFile($file, str_replace($root . '/', '', $file));
    else
    {
        // Recursively add subFiles
        foreach (getFiles($file) as $subFile)
            if (!addFileToZip($subFile, $zip, $depth - 1, $root))
                return false;

        return true;
    }
}

/**
 * @param $path string path to get files in
 * @return array of files in $path
 */
function getFiles($path)
{
    $_files = array_diff(scandir($path), array('..', '.'));
    foreach ($_files as &$_file)
        $_file = $path . '/' . $_file;
    return $_files;
}

/**
 * Terminate early and display errors
 * @param $error string String to display
 */
function displayError($error)
{
    printf('An error has occurred: <br> %s', $error);
    exit(1);
}